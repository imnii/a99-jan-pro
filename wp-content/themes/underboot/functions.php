<?php
/**
 * underboot functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package underboot
 */

if ( ! function_exists( 'underboot_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function underboot_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on underboot, use a find and replace
	 * to change 'underboot' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'underboot', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	//register_nav_menus( array(
		//'menu-1' => esc_html__( 'Primary', 'underboot' ),
	//) );
	
	// Register Bootstrap Navigation Walker, allows BS to work with WP menus
	//include '/wp_bootstrap_navwalker.php';
	include get_template_directory() . '/wp-bootstrap-navwalker-master/wp_bootstrap_navwalker.php';
	register_nav_menus( array(
		'primary-menu' => __( 'Default', 'underboot' ),
	) );
	
	

	

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'underboot_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'underboot_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function underboot_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'underboot_content_width', 640 );
}
add_action( 'after_setup_theme', 'underboot_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function underboot_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'underboot' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'underboot' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'underboot_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function underboot_scripts() {
	wp_deregister_script( 'jquery' );

	// GOOGLE FONTS
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' );

	// JQUERY
	wp_enqueue_script('jquery-latest', 'https://code.jquery.com/jquery-3.2.1.min.js');

	// FONT AWESOME
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	
	// BOOTSTRAP
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js');
	wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri().'/css/bootstrap.css' );	

	// SLICK
	wp_enqueue_script( 'slick-js', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js');
	wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css' );	

	// FANCYBOX 3
	wp_enqueue_style( 'fancybox-css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css' );	
	wp_enqueue_script( 'fancybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js');
	
	// DOUBLE TAP TO GO
	wp_enqueue_script( 'underboot-doubletaptogo', get_template_directory_uri() . '/js/doubletaptogo.js');

	// --- The WP stylesheet, put all theme specific styles HERE:
	wp_enqueue_style( 'underboot-style', get_stylesheet_uri() );

	wp_enqueue_style( 'tinymce-styles', get_template_directory_uri() . '/css/tinymce-styles.css');


	//from the underscores starter theme.  just use Bootstrap with navwalker.
	//wp_enqueue_script( 'underboot-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'underboot-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'underboot_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

//start whitelabeling
// https://premium.wpmudev.org/blog/white-labeling-wordpress/
//* Do NOT include the opening php tag
add_filter('gettext', 'change_howdy', 10, 3);
function change_howdy($translated, $text, $domain) {
    if (!is_admin() || 'default' != $domain)
        return $translated;
    if (false !== strpos($translated, 'Howdy'))
        return str_replace('Howdy', 'Unlock Your Digital Potential ', $translated);
    return $translated;
}
function change_footer_admin () {  
  echo 'Unlock Your Digital Potential with Qiigo.';  
}  
add_filter('admin_footer_text', 'change_footer_admin');
//* Change the URL of the WordPress login logo
function b3m_url_login_logo(){
    return get_bloginfo( 'wpurl' );
}
add_filter('login_headerurl', 'b3m_url_login_logo');


//remove wp logo from toolbar
//https://codex.wordpress.org/Function_Reference/remove_node
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

//minimal branding to login page:
//https://codex.wordpress.org/Customizing_the_Login_Form
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png);
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Unlock Your Digital Potential';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
// https://codex.wordpress.org/Function_Reference/remove_meta_box
function remove_dashboard_widgets() {
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );   // Right Now
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' ); // Recent Comments
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  // Incoming Links
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   // Plugins
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  // Quick Press
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  // Recent Drafts
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );   // WordPress blog
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );   // Other WordPress News
	// use 'dashboard-network' as the second parameter to remove widgets from a network dashboard.
}
add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );

// http://www.wpbeginner.com/wp-tutorials/how-to-remove-the-welcome-panel-in-wordpress-dashboard/
remove_action('welcome_panel', 'wp_welcome_panel');


//https://codex.wordpress.org/Function_Reference/register_taxonomy
// hook into the init action and call create_offernews_taxonomies when it fires
add_action( 'init', 'create_offernews_taxonomies', 0 );
// create taxonomy to tell difference between news and offer post
function create_offernews_taxonomies() {
	//may need meta_box_cb callback function name? not sure.  
	$args = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'label'				=> 'Offer, News, Testimonial, or Featured?',
		'show_in_menu'		=> true
	);

	register_taxonomy( 'offernews', 'post', $args );
}


// CONSTANTS ------------------------------------------------------------------------------------------------
define("IMAGE", get_template_directory_uri().'/img');

// CUSTOM FUNCTIONS
function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


//CUSTOM MCE ------------------------------------------------------------------------------------------------
function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function myprefix_add_format_styles( $init_array ) {
	$style_formats = array(
		// Each array child is a format with it's own settings - add as many as you want
		array(
			'title'    => 'Custom Paragraph 1',
			'selector' => 'p',
			'classes'  => 'custom-p1'
		),
		array(
			'title'    => 'Custom Paragraph 2',
			'selector' => 'p',
			'classes'  => 'custom-p2'
		)
	);
	$init_array['style_formats'] = json_encode( $style_formats );
	return $init_array;
} 
add_filter( 'tiny_mce_before_init', 'myprefix_add_format_styles' );

add_editor_style(get_template_directory_uri() . '/css/tinymce-styles.css');

// CUSTON TAXONOMY FOR PAGE------------------------------------------------------------------------------------------------
add_action( 'init', 'categoryOfCleaning');
function categoryOfCleaning() {

	register_taxonomy( 'category_of_cleaning', 'page', array(
		'hierarchical'      => true,
		'label'				=> 'Category of Cleaning',
		'rewrite'      => array( 'slug' => 'category-of-cleaning' ),
	));
}


// function create_cleaning_cat_tax() {
//     register_taxonomy( 'genre', 'book', array(
//         'label'        => __( 'Cleaning Category'),
//         'rewrite'      => array( 'slug' => 'cleaning_category' ),
//         'hierarchical' => true,
//     ) );
// }
// add_action( 'init', 'create_cleaning_cat_tax', 0 );




// CUSTOM WIDGET------------------------------------------------------------------------------------------------
// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

// Creating the widget 
class wpb_widget extends WP_Widget {

function __construct() {
parent::__construct(

// Base ID of your widget
'wpb_widget', 

// Widget name will appear in UI
__('Customed JanPro Widget', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );

// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];


// START OF OUTPUT
?>
	<ul>
		<?php 
			$location = do_shortcode('[bng_location id="location_code"]');
			$tax_terms = get_terms('offernews', array('hide_empty' => false));
			foreach($tax_terms as $term_single) {      
				if($location == 'default'){
					 $param = array(
						'posts_per_page'   => -1, 
						'tax_query' => array(
							'relation' => 'AND',
		            		array(
		            			'taxonomy' => 'category',
		            			'field'    => 'slug',
		            			'terms'    => array('blog'),
		            		),
		            		array(
		            			'taxonomy' => 'offernews',
		            			'field'    => 'slug',
		            			'terms'    => array($term_single->slug),
		            		),
		            		array(
		            			'taxonomy' => 'offernews',
		            			'field'    => 'slug',
		            			'terms'    => array('testimonials'),
		            			'operator'  => 'NOT IN'
		            		),
		            	),
					); 
				}
				else{
					$param = array(
						'posts_per_page'   => -1, 
						'tax_query' => array(
							'relation' => 'AND',
		            		array(
		            			'taxonomy' => 'category',
		            			'field'    => 'slug',
		            			'terms'    => array($location,'blog'),
		            		),
		            		array(
		            			'taxonomy' => 'offernews',
		            			'field'    => 'slug',
		            			'terms'    => array($term_single->slug),
		            		),
		            		array(
		            			'taxonomy' => 'offernews',
		            			'field'    => 'slug',
		            			'terms'    => array('testimonials'),
		            			'operator'  => 'NOT IN'
		            		),
		            	),
					); 
				}

				$the_query = new WP_Query( $param );
				if(!$the_query->found_posts == 0){ ?>
					<li><a href="<?php echo site_url() . '/archive-category?offernews=' . $term_single->slug; ?>"><?php echo $term_single->name; ?></a> (<?php echo $the_query->found_posts; ?>)</li>
				<?php
				}
			} 
		?>
	</ul>
<!--END OF OUTPUT-->
<?php
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'wpb_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
}
?>