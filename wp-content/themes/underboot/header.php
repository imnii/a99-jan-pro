<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package underboot
*/

if( !session_id()) 
session_start();

/*if($_SESSION['org_referer']=="")
{
    $_SESSION['org_referer'] = htmlspecialchars($_SERVER['HTTP_REFERER']);

}
$_SESSION['site_location'] = $site_location_name;
*/
$site_location_name = do_shortcode('[bng_location id="location_name"]');
$site = (isset($site_location_name) && !empty($site_location_name))?$site_location_name:'Jan-Pro';
$request_uri =  (isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']))?$_SERVER['REQUEST_URI']:'';



if ($_SERVER['HTTP_REFERER']=='' || (strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST'])))
{
  $ref = isset($_COOKIE['jp_referer'])?$_COOKIE['jp_referer']:'';
  
} else {
  
  $ref = htmlspecialchars($_SERVER['HTTP_REFERER']);
}


?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>

    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P2VXXB');</script>
<!-- End Google Tag Manager -->
<?php  //eliminates phone changer error in console: ?> 
<script type="text/javascript">	
function checkCookie() {

var org_ref = "<?php echo $ref; ?>";
var request_uri = "<?php echo $request_uri; ?>";
var site_location = "<?php echo $site; ?>";

var janpro_params = {jp_referer:org_ref,jp_page:request_uri,jp_location:site_location};
	for (var k in janpro_params) {
	
    var cookie_value = getCookie(k);
    if(cookie_value == '' || cookie_value!=janpro_params[k]){
    	setCookie(k,janpro_params[k], 365);
    }
    
  } 
}

function getCookie(cname) {	
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
   
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

checkCookie();


</script>

<script type="text/javascript"><!--
vs_account_id      = "";
	//--></script>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="format-detection" content="telephone=no">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php wp_head(); ?>

		<!--Qiigo Variables-->
		<?php 
			$qiigo = array(
				'location_code' => do_shortcode( '[bng_location id="location_code"]'),
				'location_name' => do_shortcode( '[bng_location id="location_name"]'),
				'primary_phone' => do_shortcode( '[bng_location id="primary_phone"]'),
				'zopim_code' => do_shortcode( '[bng_location id="zopim_code"]'),
			);
			
			
		?>
		
		<!--Start of Zopim Live Chat Script-->
		<?php if($qiigo['zopim_code']): ?>
		<script type="text/javascript">

		window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
		$.src="//v2.zopim.com/?<?php echo $qiigo['zopim_code']; ?>";z.t=+new Date;$.
		type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
		<?php endif; ?>
		<!--End of Zopim Live Chat Script-->


	</head>
	<body <?php body_class(); ?>>
   	<!-- Qiigo Q Tag number -->
		<div data-qtag-num="<?php the_field('q_tag'); ?>" hidden></div>
	<!-- END Qiigo Q Tag number -->
   
   
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2VXXB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
		<!--Page-->
		<div id="page">
			<!--Header-->
			<header class="header">
				<div>
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<!--Logo-->
								<div class="logo">

								<?php if($qiigo['location_code'] != 'default'): ?>
									<a href="<?php echo esc_url(home_url('/')) . $qiigo['location_code'] ?>" rel="home">
										<img class="img-responsive" src="<?php echo IMAGE ?>/header-logo.png" title="Jan-Pro"  alt="Jan-Pro Logo" />
									</a>
								<?php else: ?>
									<a href="<?php echo esc_url(home_url('/')) ?>" rel="home">
										<img class="img-responsive" src="<?php echo IMAGE ?>/header-logo.png" title="Jan-Pro"  alt="Jan-Pro Logo" />
									</a>
								<?php endif; ?>
									<!-- <a href="/">
										<img src="<?php echo IMAGE ?>/header-logo.png" class="img-responsive"/>
									</a> -->
									
                                    
                                    <!--Location-->                                 
									<?php if($qiigo['location_code'] != 'default'): ?>
									<div class="location">
										<?php echo $qiigo['location_name']; ?>
									</div>
									<?php endif; ?>
									
                                
                                
                                
                                </div>
								
                                
                                <!--Menu Toggle-->
								<div id="menu-toggle">
									<span></span>
									<span></span>
									<span></span>
								</div>
							</div>
							<div class="col-md-8">
								<!--Phone-->                 
								<div class="phone">
									<a href="tel:<?php echo $qiigo['primary_phone']; ?>">
										<?php echo $qiigo['primary_phone']; ?>
									</a> 
								</div>
								<!--Menu-->
								<nav class="menu" id="header-menu">
									<?php 
										wp_nav_menu(array(
											'menu'              => $qiigo['location_code'],
											'theme_location'    => 'primary-menu',
											'depth'             => 4
										)); 
									?>
								</nav>
							</div>
						</div>
					</div>
				</div>
					
				<!--Mega Menu-->
				<div class="container mega-menu">
					<span class="heading">Commercial Cleaning Services</span>
					<hr/>
					<div class="row">
						<div class="col-lg-6">
							<div class="row">
								<div class="col-md-7">
									<div>
										<span class="title">Business Cleaning</span>
										<ul class="three-list">
											<?php 
												$menu_obj = wp_get_nav_menu_items($qiigo['location_code']);

												foreach($menu_obj as $x){
													$post_meta_data = get_post_meta($x->object_id); // nah

													$menu_title = $x->post_title;
													$menu_link = get_permalink($x->object_id);
													$category_of_cleaning_id = $post_meta_data['_yoast_wpseo_primary_category_of_cleaning'][0];

													if($category_of_cleaning_id){ 
														$term = get_term($category_of_cleaning_id);
														$category_of_cleaning_slug = $term->slug
													?>
														
														<?php if($category_of_cleaning_slug == 'business-cleaning'): ?>
															<li><a href="<?= $menu_link ?>"><?= $menu_title ?></a></li>
														<?php endif; ?>
													<?php 
													}
												}
											?>
										</ul>

									</div>
								</div>
								<div class="col-md-5">
									<div>
										<span class="title">Healthcare Cleaning</span>
										<ul>
											<?php 
												$menu_obj = wp_get_nav_menu_items($qiigo['location_code']);

												foreach($menu_obj as $x){
													$post_meta_data = get_post_meta($x->object_id); // nah

													$menu_title = $x->post_title;
													$menu_link = get_permalink($x->object_id);
													$category_of_cleaning_id = $post_meta_data['_yoast_wpseo_primary_category_of_cleaning'][0];

													if($category_of_cleaning_id){ 
														$term = get_term($category_of_cleaning_id);
														$category_of_cleaning_slug = $term->slug
													?>
														
														<?php if($category_of_cleaning_slug == 'healthcare-cleaning'): ?>
															<li><a href="<?= $menu_link ?>"><?= $menu_title ?></a></li>
														<?php endif; ?>
													<?php 
													}
												}
											?>
										</ul>
									</div>
									<div>
										<span class="title special">Specialty Cleaning</span>
										<ul>
											<?php 
												$menu_obj = wp_get_nav_menu_items($qiigo['location_code']);

												foreach($menu_obj as $x){
													$post_meta_data = get_post_meta($x->object_id); // nah

													$menu_title = $x->post_title;
													$menu_link = get_permalink($x->object_id);
													$category_of_cleaning_id = $post_meta_data['_yoast_wpseo_primary_category_of_cleaning'][0];

													if($category_of_cleaning_id){ 
														$term = get_term($category_of_cleaning_id);
														$category_of_cleaning_slug = $term->slug
													?>
														
														<?php if($category_of_cleaning_slug == 'specialty-cleaning'): ?>
															<li><a href="<?= $menu_link ?>"><?= $menu_title ?></a></li>
														<?php endif; ?>
													<?php 
													}
												}
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<?php 
								$args = array(
									'posts_per_page' => 1, 
									'post_type'      => 'post',
									'post_status' => 'publish',
									'orderby' => 'post_date',
									'order' => 'DESC',
									'tax_query' => array(
										'relation' => 'AND',
										array(
											'taxonomy' => 'category',
											'field'    => 'slug',
											'terms'    => array('blog')
										)
									)
								);
							?>
							<?php $the_query = new WP_Query( $args ); ?>
							<?php 
								if ( $the_query->have_posts() ) {
									while ( $the_query->have_posts() ):
										$the_query->the_post();
							?>	
							<div class="row blog-post">
								<div class="col-md-12">
									<span class="title blue">Office Cleaning Tips</span>
								</div>
								<div class="col-md-5">
									<div class="post-img">
										<a href="<?php the_permalink(); ?>">
											<?php print_r(get_the_post_thumbnail()) ?>
										</a>
									</div>
								</div>
								<div class="col-md-7">
									<span class="heading"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></span>
									<p class="excerpt"><?php echo wp_trim_words(get_the_excerpt(), 20, $more = '') ?></p>
									<a href="<?php the_permalink() ?>" class="btn-blue">Read More Tips</a>
								</div>

							</div>
							<?php
									endwhile;
									wp_reset_postdata();
								} 
								else {
									echo "No Posts found.";
								}
							?>
						</div>
					</div>
				</div>
				<script>
					function mm() {
						var tmm_MM = $(".toggle-mega-menu, .mega-menu"),
							megaMenu = $(".mega-menu"),
							mobileW = 991,
							originalSub = $(".toggle-mega-menu .sub-menu");

						megaMenu.hide();
						originalSub.css('opacity', '0');

						$(window).on('resize', function() {
							if ($(window).innerWidth() >= mobileW) {
								originalSub.css('opacity', '0');
								tmm_MM.hover(
									function() {
										megaMenu.show();
									},
									function() {
										megaMenu.hide();
									}
								);
							} 
							else {
								megaMenu.hide();
								originalSub.css('opacity', '1');
							}
						});
						$(window).trigger('resize');
					}
					mm();
				</script>
				<!--End of Mega Menu-->
			</header>
			<script>
				$(function () {
					$("#header-menu").doubleTapToGo();
				});
				$(document).ready(function(){
					$('#menu-toggle').click(function(){
						$(this).toggleClass('open');
						$("#header-menu").slideToggle('fast');
					});
				});
			</script>
			<!--End of Header-->

			<!--Zip Locator-->
			<?php if($qiigo['location_code'] == 'default'): ?>
			<section class="zip-locator">
				<div>
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<!--Text Description-->
								<div class="free-consultation">
									REQUEST A CONSULTATION : <b><?php echo $qiigo['primary_phone']; ?></b> or
								</div>
							</div>
							<div class="col-lg-6">
								<!--Zip Locator Form-->
								<div class="form-wrapper">
									<form action="/" method="POST">
										<select onchange="getCountry()" id="ddlCountry" name="ddlCountry">
											<option value="http://www.jan-pro.com.au/">Australia</option>
											<option value="http://www.empresadeservicosdelimpeza.com.br/">Brazil</option>
											<option value="">Canada</option>
											<option value="http://india.jan-pro.com/">India</option>
											<option value="http://jan-pro.mx/">Mexico</option>
											<option value="http://www.jan-pro.com.au/">New Zealand</option>
											<option value="http://nigeria.jan-pro.com/">Nigeria</option>
											<option value="http://jan-pro.pl/">Poland</option>
											<option value="http://puertorico.jan-pro.com/">Puerto Rico</option>
											<option value="http://jan-pro.com/saudiaarabia/">Saudi Arabia</option>
											<option value="http://www.jan-pro.co.za/">South Africa</option>
											<option value="http://extranet.jan-pro.com/uae/">United Arab Emirates</option>
											<option value="http://jan-pro.com/unitedkingdom/">United Kingdom</option>
											<option value="" selected="selected">United States</option>
										</select>
										<script type="text/javascript">
											function getCountry(){
											if(document.getElementById('ddlCountry').value!='')
											window.location = document.getElementById('ddlCountry').value;
											}
										</script>
										<input type="text" name="location_search" placeholder="Enter Zip Code">
										<input type="submit" value="GO">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php else: ?>
			<section class="zip-locator">
				<div>
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<!--Text Description-->
								<div class="text-wrapper">
									<b><?php echo $qiigo['primary_phone']; ?></b> or
								</div>
							</div>
							<div class="col-sm-6">
								<div class="text-wrapper2">
									<a href="#" class="btn-request-consultation" data-toggle="modal" data-target="#myModal">GET A FREE ESTIMATE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php endif; ?>
			<script>
				$(window).scroll(function(){
					var headerHeight = $('.header').outerHeight() || 0;
					var zipLocatorHeight = $('.zip-locator').outerHeight() || 0;
					var wpAdminBarHeight = $('#wpadminbar').outerHeight() || 0;
					var banner = $('.banner');

					<?php if( is_admin_bar_showing()): ?>
						var totalHeaderHeight = headerHeight + wpAdminBarHeight;
						var totalMarginTop = zipLocatorHeight - wpAdminBarHeight;
					<?php else: ?>
						var totalHeaderHeight = headerHeight;
						var totalMarginTop = zipLocatorHeight;
					<?php endif; ?>
					if ($(this).scrollTop() > headerHeight) {
						$('.zip-locator').addClass('zip-locator-fixed');
						$('body').css('margin-top', totalMarginTop);
					} 
					else {
						$('.zip-locator').removeClass('zip-locator-fixed');
						$('body').css('margin-top','0px');
					}
				});
			</script>
			<!--End of Zip Locator-->

			<!--Banner-->
			<?php 
				if(is_page_template('templates/home.php')){
					get_template_part('section-parts/corp-home-banner');
				}
				elseif( (is_page_template('templates/local-home.php')) && (!get_field('banner_content') && !get_field('banner_button'))){
					get_template_part('section-parts/local-home-banner');
				}
				else{
					get_template_part('section-parts/normal-banner');
				}
			?>
			<script type="text/javascript">
				$(document).ready(function(){
					$('.banner-slider').slick({
						arrows: true,
						dots: true,
						infinite: true,
						nextArrow: '<button class="slick-btn slick-next"><i class="fa fa-chevron-right"></i></button>',
        				prevArrow: '<button class="slick-btn slick-prev"><i class="fa fa-chevron-left"></i></button>',
        				draggable: false,
        				swipe: false,
        				autoplay: true,
  						autoplaySpeed: 5000,
					});
				});

				$(".home-banner-section .banner-slider .slick-next").hide();
				$(".home-banner-section .banner-slider .slick-prev").hide();
				$(".home-banner-section .banner-slider ul.slick-dots").hide();
				

				$('.home-banner-section').hover(function() {
					$(".home-banner-section .banner-slider .slick-next").show();
					$(".home-banner-section .banner-slider .slick-prev").show();
					$(".home-banner-section .banner-slider ul.slick-dots").show();
				}, function() {
					$(".home-banner-section .banner-slider .slick-next").hide();
					$(".home-banner-section .banner-slider .slick-prev").hide();
					$(".home-banner-section .banner-slider ul.slick-dots").hide();
				})
			</script>
			<!--End of Banner-->
			
			<!--Page Title-->
			<?php if( get_field('h1_page_title') ): ?>
				<h1 class="center" ><?php the_field("h1_page_title"); ?></h1>
			<? elseif ( get_field('home_h1_page_title') ): ?>
			<div class="bg-blue">
				<div class="container content01">
					<div class="row">
					<div class="col-md-2"></div>
						<div class="col-md-8 text-center">
							<h1 class="home-h1-class" ><?php the_field("home_h1_page_title"); ?></h1>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<!--End of Page Title-->


			<!--Get Free Estimate-->
			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Get a free estimate</h4>
						</div>
						<div class="modal-body">
							<!-- qiigoforms.com script begins here -->
							<script type="text/javascript" defer src="//qiigoforms.com/embed/2961797.js" data-role="form" data-default-width="100%"></script>
							<!-- qiigoforms.com script ends here -->
						</div>
						<div class="modal-footer"><br></div>
					</div>
				</div>
			</div>
			<!--End of Free Estimate-->
			 
